import viewReducer, { ViewState } from '../View';

describe('view reducer', () => {
  it('correctly changes state', () => {
    const prevState: ViewState = {
      ratesOverTime: {
        order: 'descending',
      },
    };

    const newState = viewReducer(prevState, { type: 'set-rates-for-date-order', order: 'ascending' });

    expect(newState.ratesOverTime.order).toBe('ascending');
  });
});
