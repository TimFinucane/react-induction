import exchangesReducer, { ExchangesState, RatesForPeriodQuery, RatesForDateQuery } from '../Exchanges';

describe('store/exchanges', () => {
  it('should accept a query change for a period', () => {
    const prevState: ExchangesState = {
      ratesForDate: {
        baseCurrency: 'AUD',
        comparisonCurrencies: ['BGN'],
        date: new Date(),
        exchangeRates: [],
      },
      ratesForPeriod: {
        baseCurrency: 'AUD',
        comparisonCurrency: 'BRL',
        startDate: new Date('09 March 2020 15:05 UTC'),
        endDate: new Date('27 March 2020 15:15 UTC'),
        exchangeRates: [],
      },
    };

    const newRatesForPeriod: RatesForPeriodQuery = {
      baseCurrency: 'DKK',
      comparisonCurrency: 'ISK',
      startDate: new Date('19 March 2020 11:05 UTC'),
      endDate: new Date('26 March 2020 12:15 UTC'),
      exchangeRates: [],
    };

    const nextState = exchangesReducer(prevState, { type: 'set-period-query', query: newRatesForPeriod });

    expect(nextState.ratesForDate).toEqual(prevState.ratesForDate);
    expect(nextState.ratesForPeriod).toEqual(newRatesForPeriod);
  });

  it('should accept a query change for a specific date', () => {
    const prevState: ExchangesState = {
      ratesForDate: {
        baseCurrency: 'PLN',
        comparisonCurrencies: ['RON'],
        date: new Date(),
        exchangeRates: [],
      },
      ratesForPeriod: {
        baseCurrency: 'SGD',
        comparisonCurrency: 'TRY',
        startDate: new Date('09 March 2020 15:05 UTC'),
        endDate: new Date('27 March 2020 15:15 UTC'),
        exchangeRates: [],
      },
    };

    const newRatesForDate: RatesForDateQuery = {
      baseCurrency: 'RUB',
      comparisonCurrencies: ['TRY'],
      date: new Date('19 March 2020 11:05 UTC'),
      exchangeRates: [],
    };

    const nextState = exchangesReducer(prevState, { type: 'set-date-query', query: newRatesForDate });

    expect(nextState.ratesForPeriod).toEqual(prevState.ratesForPeriod);
    expect(nextState.ratesForDate).toEqual(newRatesForDate);
  });
});
