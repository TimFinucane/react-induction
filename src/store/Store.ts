import {
  combineReducers, createStore, applyMiddleware,
} from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';

import exchangesReducer from './Exchanges';
import viewReducer from './View';

export const reducers = combineReducers({
  exchanges: exchangesReducer,
  view: viewReducer,
});

const store = createStore(
  reducers,
  composeWithDevTools(applyMiddleware(thunk)),
);
export default store;

export type StoreState = ReturnType<typeof reducers>;
export type StoreDispatch = typeof store.dispatch;
