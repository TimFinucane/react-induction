import { StoreDispatch } from 'store/Store';
import { getExchangeRatesForDate, getExchangeRatesForPeriod } from 'api';
import { Currency } from 'api/Currencies';

/**
 * Updates store state with the new query, after getting the
 * exchange rates for that query
 *
 * @param date
 * @param baseCurrency
 * @param comparisonCurrencies
 *
 * @throws If there was a problem retrieving data from the api.
 *
 * @returns A function that aborts the op
 */
export const updateRatesForDateQuery = (
  date: Date,
  baseCurrency: Currency,
) => async (dispatch: StoreDispatch) => {
  const exchangeRates = await getExchangeRatesForDate(date, baseCurrency);

  dispatch({
    type: 'set-date-query',
    query: {
      date,
      baseCurrency,
      exchangeRates,
    },
  });
};

/**
 * Updates store state with the new query, after getting the
 * exchange rates for that query
 *
 * @param startDate
 * @param endDate
 * @param baseCurrency
 * @param comparisonCurrency
 *
 * @throws If there was a problem retrieving data from the api.
 *
 * @returns A function that aborts the op
 */
export const updateRatesForPeriodQuery = (
  baseCurrency: Currency,
  comparisonCurrency: Currency,
  startDate: Date,
  endDate: Date,
) => async (dispatch: StoreDispatch) => {
  const didCancel = false;

  const exchangeRates = await getExchangeRatesForPeriod(
    baseCurrency,
    comparisonCurrency,
    startDate,
    endDate,
  );

  if (didCancel) {
    return;
  }

  dispatch({
    type: 'set-period-query',
    query: {
      startDate,
      endDate,
      baseCurrency,
      comparisonCurrency,
      exchangeRates,
    },
  });
};
