
export interface ViewState {
  ratesOverTime: {
    order: ViewOrder;
  }
}

export type ViewOrder = 'ascending' | 'descending';

const initialState: ViewState = {
  ratesOverTime: {
    order: 'descending',
  },
};

export type ViewActions = { type: 'set-rates-for-date-order', order: ViewOrder };

export default function viewReducer(
  state: ViewState = initialState,
  action: ViewActions,
): ViewState {
  switch (action.type) {
    case 'set-rates-for-date-order':
      return {
        ratesOverTime: {
          order: action.order,
        },
      };
    default:
      return state;
  }
}

export const setRatesForDateOrder = (order: ViewOrder): ViewActions => ({ type: 'set-rates-for-date-order', order });
