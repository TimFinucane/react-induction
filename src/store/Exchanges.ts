import { Currency } from 'api/Currencies';
import { ExchangeRatesForDateResult, ExchangeRatesForPeriodResult } from 'api';

export interface RatesForDateQuery {
  date: Date,
  baseCurrency: Currency;
  exchangeRates: ExchangeRatesForDateResult;
}

export interface RatesForPeriodQuery {
  startDate: Date;
  endDate: Date;
  baseCurrency: Currency;
  comparisonCurrency: Currency;
  exchangeRates: ExchangeRatesForPeriodResult;
}

export interface ExchangesState {
  ratesForDate?: RatesForDateQuery;
  ratesForPeriod?: RatesForPeriodQuery;
}

type ExchangesActions =
  | { type: 'set-date-query', query: RatesForDateQuery }
  | { type: 'set-period-query', query: RatesForPeriodQuery };

const initialState: ExchangesState = {
  ratesForDate: undefined,
  ratesForPeriod: undefined,
};

/**
 * @param state State, defaults to initial state, which is an empty object
 * @param action The action being run
 *
 * @returns The query state
 */
export default function exchangesReducer(
  state: ExchangesState = initialState,
  action: ExchangesActions,
) : ExchangesState {
  switch (action.type) {
    case 'set-date-query':
      return {
        ...state,
        ratesForDate: action.query,
      };
    case 'set-period-query':
      return {
        ...state,
        ratesForPeriod: action.query,
      };
    default:
      return state;
  }
}

export const setDateQuery = (query: RatesForDateQuery): ExchangesActions => ({ type: 'set-date-query', query });
export const setPeriodQuery = (query: RatesForPeriodQuery): ExchangesActions => ({ type: 'set-period-query', query });
