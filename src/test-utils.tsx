// test-utils.js
import React, { PropsWithChildren } from 'react';
import { render as rtlRender, RenderOptions } from '@testing-library/react';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import mainStore, { reducers, StoreState } from 'store/Store';

interface RenderStoreOptions extends RenderOptions {
  initialState?: StoreState;
  store?: typeof mainStore;
}

function render(
  ui: React.ReactElement,
  {
    initialState,
    store = createStore(reducers, initialState),
    ...renderOptions
  }: RenderStoreOptions = {},
) {
  function Wrapper({ children }: PropsWithChildren<{}>) {
    return <Provider store={store}>{children}</Provider>;
  }
  return rtlRender(ui, { wrapper: Wrapper, ...renderOptions });
}

// re-export everything
export * from '@testing-library/react';

// override render method
export { render };
