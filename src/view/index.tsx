import React from 'react';
import {
  Switch, Route, BrowserRouter, Redirect,
} from 'react-router-dom';
import Container from '@material-ui/core/Container';

import NavBar from './NavBar';
import ExchangeRatesForDay from './pages/ExchangeRatesForDay';
import ExchangeRatesBetweenTwo from './pages/ExchangeRatesBetweenTwo';
import NotFound from './pages/NotFound';


export default function App() {
  return (
    <Container>
      <BrowserRouter>
        <NavBar options={[{ url: '/date', name: 'For Day' }, { url: '/recent', name: 'Between Two Currencies' }]} />
        {/*
          * The default route should be /recent,
          * but any other route should go to the NotFound page.
          */}
        <Switch>
          <Redirect exact from="/" to="/date" />
          <Route path="/date">
            <ExchangeRatesForDay />
          </Route>
          <Route path="/recent">
            <ExchangeRatesBetweenTwo />
          </Route>
          <Route>
            <NotFound />
          </Route>
        </Switch>
      </BrowserRouter>
    </Container>
  );
}
