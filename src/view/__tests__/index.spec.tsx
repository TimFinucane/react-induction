import React from 'react';
import { render } from 'test-utils';

import App from '..';

describe('Root view', () => {
  it('will render', () => {
    render(<App />);
  });
});
