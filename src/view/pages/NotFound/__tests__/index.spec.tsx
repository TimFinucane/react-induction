import React from 'react';
import { render } from '@testing-library/react';

import NotFound from '..';

describe('Not Found page', () => {
  it('should say "Page Not Found"', () => {
    const { getByText } = render(<NotFound />);

    const element = getByText('Page Not Found');

    expect(element).toBeDefined();
  });
});
