import React from 'react';
import {
  render,
  act,
  fireEvent,
  waitFor,
  waitForElementToBeRemoved,
} from 'test-utils';
import { mocked } from 'ts-jest/utils';

import { getExchangeRatesForPeriod, ExchangeRatesForPeriodResult } from 'api';
import ExchangeRatesBetweenTwo from '..';

jest.mock('view/common/CurrencyPicker.tsx');
jest.mock('api');

describe('Rates For Date container', () => {
  beforeEach(() => {
    jest.clearAllMocks();
    mocked(getExchangeRatesForPeriod)
      .mockResolvedValue([]);
  });

  it('should immediately query api', () => {
    const getRates = mocked(getExchangeRatesForPeriod);

    render(<ExchangeRatesBetweenTwo />);

    expect(getRates).toHaveBeenCalled();
  });

  it('will re-query when values change', async () => {
    const getRates = mocked(getExchangeRatesForPeriod);

    const { getByLabelText } = render(<ExchangeRatesBetweenTwo />);

    act(() => {
      fireEvent.change(getByLabelText('Base Currency'), { target: { value: 'EUR' } });
    });

    expect(getRates).toHaveBeenLastCalledWith('EUR', expect.any(String), expect.any(Date), expect.any(Date));
  });

  it('will display the spinner while loading', async () => {
    let resolver: (rates: ExchangeRatesForPeriodResult) => void;

    const fetchMock = mocked(getExchangeRatesForPeriod);
    fetchMock.mockReturnValue(new Promise((res) => { resolver = res; }));

    const { queryByTestId, queryByText } = render(
      <ExchangeRatesBetweenTwo />,
    );

    expect(queryByTestId('loading-spinner')).toBeInTheDocument();

    // Once resolved, should be undefined
    act(() => resolver([{ date: new Date('2020/06/10'), value: 1.0 }]));
    await waitFor(() => expect(queryByText('US dollar')).toBeInTheDocument());

    expect(queryByTestId('loading-spinner')).not.toBeInTheDocument();
  });

  it('will display an error if one happened', () => {
    const fetchMock = mocked(getExchangeRatesForPeriod);
    fetchMock.mockRejectedValue(new Error('loading failed for testing reasons'));

    const { queryByText } = render(
      <ExchangeRatesBetweenTwo />,
    );

    expect(queryByText(/loading failed for testing reasons/)).toBeDefined();
  });

  it('will not let an older fetch overwrite a newer one', async () => {
    let firstResolver: (rates: ExchangeRatesForPeriodResult) => void;
    let secondResolver: (rates: ExchangeRatesForPeriodResult) => void;

    const fetchMock = mocked(getExchangeRatesForPeriod);
    fetchMock
      .mockReturnValueOnce(new Promise((res) => { firstResolver = res; }))
      .mockReturnValueOnce(new Promise((res) => { secondResolver = res; }));

    const { queryByText, getByLabelText, queryByTestId } = render(
      <ExchangeRatesBetweenTwo />,
    );

    // First resolver will have started by this point, but not resolved.

    act(() => {
      fireEvent.change(getByLabelText('Base Currency'), { target: { value: 'GBP' } });
    });

    // Now second resolver will have started

    // Now resolve the second fetch first

    // @ts-ignore - secondResolver HAS been defined.
    if (!firstResolver || !secondResolver) {
      throw new Error('Logic incorrect, both resolvers should be defined');
    }

    secondResolver([{ date: new Date('2020/06/10'), value: 1.0 }]);
    await waitForElementToBeRemoved(queryByTestId('loading-spinner'));

    expect(queryByText(new Date('2020/06/10').toLocaleDateString())).toBeInTheDocument();

    firstResolver([{ date: new Date('2020/06/09'), value: 0.5 }]);
    await new Promise((res) => setTimeout(res, 200));

    expect(queryByText(new Date('2020/06/09').toLocaleDateString())).not.toBeInTheDocument();
    expect(queryByText(new Date('2020/06/10').toLocaleDateString())).toBeInTheDocument();
  });

  it('will not let an older fetch overwrite an error on a newer one', async () => {
    let firstResolver: (error: Error) => void;
    let secondResolver: (rates: ExchangeRatesForPeriodResult) => void;

    const fetchMock = mocked(getExchangeRatesForPeriod);
    fetchMock
      .mockReturnValueOnce(new Promise((res, rej) => { firstResolver = rej; }))
      .mockReturnValueOnce(new Promise((res) => { secondResolver = res; }));

    const { queryByText, getByLabelText, queryByTestId } = render(
      <ExchangeRatesBetweenTwo />,
    );

    // First resolver will have started by this point, but not resolved.

    act(() => {
      fireEvent.change(getByLabelText('Base Currency'), { target: { value: 'GBP' } });
    });

    // Now second resolver will have started

    // Now resolve the second fetch first

    // @ts-ignore - secondResolver HAS been defined.
    if (!firstResolver || !secondResolver) {
      throw new Error('Logic incorrect, both resolvers should be defined');
    }

    secondResolver([{ date: new Date('2020/06/10'), value: 1.0 }]);
    await waitForElementToBeRemoved(queryByTestId('loading-spinner'));

    expect(queryByText(new Date('2020/06/10').toLocaleDateString())).toBeInTheDocument();

    firstResolver(new Error('This should not be shown'));
    await new Promise((res) => setTimeout(res, 200));

    expect(queryByText(/This should not be shown/)).not.toBeInTheDocument();
    expect(queryByText(new Date('2020/06/10').toLocaleDateString())).toBeInTheDocument();
  });
});
