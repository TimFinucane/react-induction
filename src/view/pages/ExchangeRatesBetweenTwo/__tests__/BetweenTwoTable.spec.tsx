/* eslint-disable complexity */
import React from 'react';
import { render, fireEvent } from 'test-utils';

import { act } from 'react-dom/test-utils';

import BetweenTwoTable from '../BetweenTwoTable';

jest.mock('view/common/Picker');

describe('BetweenTwoTable Presentation', () => {
  it('should correctly order items', () => {
    const rows = [
      { date: new Date(), value: 0.11 },
      { date: new Date(Date.now() - 24 * 60 * 60 * 1000), value: 0.04 },
    ];

    const { getByText, getByLabelText } = render(
      <BetweenTwoTable currency="USD" exchangeRates={rows} />,
    );

    // Note: This is a brittle way of comparing order
    let first = getByText(rows[0].date.toLocaleDateString()).parentNode;
    let second = getByText(rows[1].date.toLocaleDateString()).parentNode;

    let parent = first?.parentNode;
    if (!first || !second || !parent) {
      fail('Expected date elements to be in item which itself has parent');
    }
    if (first.parentNode !== second.parentElement) {
      fail('Expected parents of each row to share the same parent');
    }

    let firstIndex = Array.prototype.indexOf.call(parent.childNodes, first);
    let secondIndex = Array.prototype.indexOf.call(parent.childNodes, second);
    expect(firstIndex).toBeLessThan(secondIndex);

    act(() => {
      fireEvent.change(getByLabelText('Order Dates By'), { target: { value: 'ascending' } });
    });

    first = getByText(rows[0].date.toLocaleDateString()).parentNode;
    second = getByText(rows[1].date.toLocaleDateString()).parentNode;

    parent = first?.parentNode;
    if (!first || !second || !parent) {
      fail('Expected date elements to be in item which itself has parent');
    }
    if (first.parentNode !== second.parentElement) {
      fail('Expected parents of each row to share the same parent');
    }

    firstIndex = Array.prototype.indexOf.call(parent.childNodes, first);
    secondIndex = Array.prototype.indexOf.call(parent.childNodes, second);

    expect(secondIndex).toBeLessThan(firstIndex);
  });
});
