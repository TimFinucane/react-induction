import React from 'react';
import { render, act, fireEvent } from '@testing-library/react';

import currencies, { Currency } from 'api/Currencies';
import BetweenTwoForm from '../BetweenTwoForm';

jest.mock('view/common/CurrencyPicker.tsx');
jest.mock('view/common/Picker');

describe('Rates-Over-Time form', () => {
  it('will display entered values in long form', () => {
    const { getByText } = render(
      <BetweenTwoForm baseCurrency="EUR" comparisonCurrency="USD" onChange={() => {}} />,
    );

    expect(getByText(currencies.EUR)).toBeTruthy();
    expect(getByText(currencies.USD)).toBeTruthy();
  });

  it('will fire onChange when selection changes', () => {
    const onChange = jest.fn<void, Currency[]>();

    const { getByLabelText } = render(
      <BetweenTwoForm baseCurrency="EUR" comparisonCurrency="USD" onChange={onChange} />,
    );

    const basePicker = getByLabelText('Base Currency');
    const comparisonPicker = getByLabelText('Compare To');

    act(() => {
      fireEvent.change(basePicker, { target: { value: 'BRL' } });
    });

    expect(onChange).toHaveBeenCalledWith('BRL', 'USD');

    act(() => {
      fireEvent.change(comparisonPicker, { target: { value: 'AUD' } });
    });

    // Keep in mind, props haven't changed
    expect(onChange).toHaveBeenLastCalledWith('EUR', 'AUD');
  });
});
