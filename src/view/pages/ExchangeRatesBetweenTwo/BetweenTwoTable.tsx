import React from 'react';
import { connect } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';

import { Currency } from 'api/Currencies';
import { ViewOrder, setRatesForDateOrder } from 'store/View';
import { displayCurrencyValue } from 'view/TypeDisplays';
import Picker from 'view/common/Picker';
import Table from 'view/common/Table';
import { StoreState } from 'store/Store';

const useStyles = makeStyles((theme) => ({
  datePicker: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(2),
  },
}));

interface Props {
  currency: Currency;
  exchangeRates: { date: Date; value: number }[];

  order: ViewOrder;
  setOrder(order: ViewOrder): void;
}

function InnerBetweenTwoTable({
  currency,
  exchangeRates,
  order,
  setOrder,
}: Props) {
  // Convert the rows to a human-readable form, and order them by orderDate.
  let displayRows: [string, string][] = exchangeRates
    .sort(({ date: dateA }, { date: dateB }) => dateB.getTime() - dateA.getTime())
    .map(({ date, value }) => [date.toLocaleDateString(), displayCurrencyValue(value, currency)]);

  if (order === 'ascending') {
    displayRows = displayRows.reverse();
  }

  const styles = useStyles();

  return (
    <>
      <Box className={styles.datePicker}>
        <Picker
          id="order-date-picker"
          label="Order Dates By"
          options={[{ value: 'descending' }, { value: 'ascending' }]}
          value={order}
          setValue={(option) => setOrder(option)}
        />
      </Box>
      <Table columns={['Date', 'Value']} rows={displayRows} />
    </>
  );
}

const mapStateToProps = (state: StoreState) => ({
  order: state.view.ratesOverTime.order,
});
const mapDispatchToProps = {
  setOrder: setRatesForDateOrder,
};

const BetweenTwoTable = connect(mapStateToProps, mapDispatchToProps)(InnerBetweenTwoTable);
export default BetweenTwoTable;
