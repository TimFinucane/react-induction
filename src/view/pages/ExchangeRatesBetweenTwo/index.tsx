import React, { useState, useEffect, useCallback } from 'react';
import { connect } from 'react-redux';
import CircularProgress from '@material-ui/core/CircularProgress';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';

import { ExchangeRatesForPeriodResult } from 'api';
import { Currency } from 'api/Currencies';
import { StoreState } from 'store/Store';
import { updateRatesForPeriodQuery } from 'store/thunks/ApiActions';
import ErrorMessage from 'view/common/ErrorMessage';
import BetweenTwoForm from './BetweenTwoForm';
import BetweenTwoTable from './BetweenTwoTable';

const useStyles = makeStyles((theme) => ({
  heading: {
    textAlign: 'center',
  },
  actionContainer: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
}));

const defaultBaseCurrency: Currency = 'EUR';
const defaultComparisonCurrency: Currency = 'USD';
const defaultStartDate = new Date(Date.now() - 5 * 24 * 60 * 60 * 1000);
const defaultEndDate = new Date();

interface Props {
  baseCurrency?: Currency;
  comparisonCurrency?: Currency;
  startDate?: Date;
  endDate?: Date;
  update(
    baseCurrency: Currency,
    comparisonCurrency: Currency,
    startDate: Date,
    endDate: Date,
  ): void;
}

function InnerExchangeRatesBetweenTwo({
  baseCurrency,
  comparisonCurrency,
  startDate,
  endDate,
  update,
}: Props) {
  const base = baseCurrency ?? defaultBaseCurrency;
  const comparison = comparisonCurrency ?? defaultComparisonCurrency;
  const start = startDate ?? defaultStartDate;
  const end = endDate ?? defaultEndDate;

  const [exchangeRates, setExchangeRates] = useState<ExchangeRatesForPeriodResult>([]);
  const [error, setError] = useState<string | undefined>(undefined);
  const [isLoading, setLoading] = useState(false);

  const loadExchangeRates = useCallback(async (
    newBase: Currency,
    newComparison: Currency,
    newStart: Date,
    newEnd: Date,
  ) => {
    setExchangeRates([]);
    setError(undefined);
    setLoading(true);

    try {
      await update(newBase, newComparison, newStart, newEnd);
    } catch (e) {
      setError(e instanceof Error ? e.message : e.toString());
    }
    setLoading(false);
  }, [update]);

  // Ensure the values are initialized
  useEffect(() => {
    if (exchangeRates === undefined) {
      loadExchangeRates(base, comparison, start, end);
    }
  }, [base, comparison, start, end, exchangeRates, loadExchangeRates]);

  const styles = useStyles();
  return (
    <>
      <h1 className={styles.heading}>Exchange Rates Over Time</h1>
      <BetweenTwoForm
        baseCurrency={base}
        comparisonCurrency={comparison}
        onChange={(nextBase, nextComparison) => loadExchangeRates(
          nextBase,
          nextComparison,
          start,
          end,
        )}
      />
      <BetweenTwoTable
        currency={comparison}
        exchangeRates={exchangeRates}
      />
      <Grid container direction="column" justify="center" alignItems="center" className={styles.actionContainer}>
        {error
          && <ErrorMessage>{error}</ErrorMessage>}
        {isLoading
          && <CircularProgress data-testid="loading-spinner" />}
      </Grid>
    </>
  );
}

const mapStateToProps = (store: StoreState) => ({
  ...store.exchanges.ratesForPeriod,
});
const mapDispatchToProps = {
  update: updateRatesForPeriodQuery,
};

const ExchangeRatesBetweenTwo = connect(
  mapStateToProps,
  mapDispatchToProps,
)(InnerExchangeRatesBetweenTwo);
export default ExchangeRatesBetweenTwo;
