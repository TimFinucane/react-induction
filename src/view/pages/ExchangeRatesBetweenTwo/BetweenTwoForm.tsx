import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';

import { Currency } from 'api/Currencies';
import CurrencyPicker from 'view/common/CurrencyPicker';

const useStyles = makeStyles({
  versusText: {
    verticalAlign: 'center',
  },
});

interface Props {
  baseCurrency: Currency;
  comparisonCurrency: Currency;

  onChange(baseCurrency: Currency, comparisonCurrency: Currency): void;
}

export default function BetweenTwoForm({ baseCurrency, comparisonCurrency, onChange }: Props) {
  const styles = useStyles();

  return (
    <Grid container direction="row" justify="center" alignItems="center" spacing={1}>
      <Grid item>
        <CurrencyPicker
          id="base-currency-picker"
          label="Base Currency"
          currency={baseCurrency}
          setCurrency={(value) => onChange(value, comparisonCurrency)}
        />
      </Grid>
      <Grid item>
        <p className={styles.versusText}>v</p>
      </Grid>
      <Grid item>
        <CurrencyPicker
          id="comparison-currency-picker"
          label="Compare To"
          currency={comparisonCurrency}
          setCurrency={(value) => onChange(baseCurrency, value)}
        />
      </Grid>
    </Grid>
  );
}
