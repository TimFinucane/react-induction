import React, { useState, useEffect, useCallback } from 'react';
import { connect } from 'react-redux';
import Container from '@material-ui/core/Container';
import CircularProgress from '@material-ui/core/CircularProgress';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';

import { Currency } from 'api/Currencies';
import { ExchangeRatesForDateResult } from 'api';
import { StoreState } from 'store/Store';
import ErrorMessage from 'view/common/ErrorMessage';
import { updateRatesForDateQuery } from 'store/thunks/ApiActions';
import ForDayForm from './ForDayForm';
import ForDayTable from './ForDayTable';

const useStyles = makeStyles((theme) => ({
  heading: {
    textAlign: 'center',
  },
  actionContainer: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
}));

const defaultBaseCurrency = 'NZD';
const defaultDate = new Date();

interface Props {
  baseCurrency?: Currency;
  date?: Date;
  update(date: Date, baseCurrency: Currency): void;
}

function InnerExchangeRatesForDay({ update, baseCurrency, date: propsDate }: Props) {
  const base = baseCurrency ?? defaultBaseCurrency;
  const date = propsDate ?? defaultDate;

  const [exchangeRates, setExchangeRates] = useState<ExchangeRatesForDateResult>([]);
  const [error, setError] = useState<string | undefined>();
  const [isLoading, setLoading] = useState(false);

  // Can't define the useEffect function as async.
  const loadExchangeRates = useCallback(async (newDate: Date, newBase: Currency) => {
    setExchangeRates([]);
    setError(undefined);
    setLoading(true);

    try {
      await update(newDate, newBase);
    } catch (e) {
      setError(e instanceof Error ? e.message : e.toString());
    }
    setLoading(false);
  }, [update]);

  useEffect(() => {
    if (exchangeRates === undefined) {
      loadExchangeRates(date, base);
    }
  }, [date, base, exchangeRates, loadExchangeRates]);

  const styles = useStyles();

  return (
    <Container>
      <h1 className={styles.heading}>Exchange Rates for a Day</h1>
      <ForDayForm
        currency={base}
        date={date}
        setQuery={(nextCurrency, nextDate) => loadExchangeRates(nextDate, nextCurrency)}
      />
      <ForDayTable exchangeRates={exchangeRates} />
      <Grid container direction="column" justify="center" alignItems="center" className={styles.actionContainer}>
        {error
        && <ErrorMessage>{error}</ErrorMessage>}
        {isLoading
        && <CircularProgress data-testid="loading-spinner" />}
      </Grid>
    </Container>
  );
}

const mapStateToProps = (state: StoreState) => ({
  ...state.exchanges.ratesForDate,
});
const mapDispatchToProps = {
  update: updateRatesForDateQuery,
};

const ExchangeRatesForDay = connect(mapStateToProps, mapDispatchToProps)(InnerExchangeRatesForDay);
export default ExchangeRatesForDay;
