import React from 'react';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';

import { Currency } from 'api/Currencies';
import CurrencyPicker from 'view/common/CurrencyPicker';
import DatePicker from 'view/common/DatePicker';

const useStyles = makeStyles((theme) => ({
  container: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
}));

interface Props {
  currency: Currency;
  date: Date;

  setQuery(currency: Currency, date: Date): void;
}

export default function ForDayForm({ currency, date, setQuery }: Props) {
  const styles = useStyles();

  return (
    <Grid
      container
      direction="row"
      justify="center"
      spacing={2}
      className={styles.container}
    >
      <Grid item>
        <CurrencyPicker
          label="Base Currency"
          currency={currency}
          setCurrency={(nextCurrency) => setQuery(nextCurrency, date)}
        />
      </Grid>
      <Grid item>
        <DatePicker
          label="Date"
          date={date}
          setDate={(nextDate) => setQuery(currency, nextDate)}
        />
      </Grid>
    </Grid>
  );
}
