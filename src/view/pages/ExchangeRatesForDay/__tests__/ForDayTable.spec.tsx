import React from 'react';
import { render } from '@testing-library/react';

import ForDayTable from '../ForDayTable';

describe('RatesForDateTable Presentation', () => {
  it('should sort the table contents', () => {
    const { getByText } = render(<ForDayTable
      exchangeRates={[
        { currency: 'CAD', value: 0.5 },
        { currency: 'BRL', value: 1.0 },
      ]}
    />);
    const brlName = getByText('Brazilian real');
    const cadName = getByText('Canadian dollar');

    const list = brlName.parentNode?.parentNode;

    if (!list) {
      throw new Error('Expected brl and cad to have a list grandparent');
    }

    expect(Array.prototype.indexOf.call(list.childNodes, brlName.parentNode)).toBeLessThan(
      Array.prototype.indexOf.call(list.childNodes, cadName.parentNode),
    );
  });
});
