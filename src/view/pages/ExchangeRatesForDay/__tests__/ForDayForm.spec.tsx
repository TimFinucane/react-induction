/* eslint-disable max-nested-callbacks */
import React from 'react';
import {
  render,
  act,
  fireEvent,
} from '@testing-library/react';

import currencies, { Currency } from 'api/Currencies';
import RatesForDateForm from '../ForDayForm';

jest.mock('view/common/CurrencyPicker.tsx');
jest.mock('view/common/DatePicker.tsx');

describe('For Day Form', () => {
  it('should display specified currency in long form', () => {
    const date = new Date('2020/04/01');
    const setQuery = jest.fn();
    const currency: Currency = 'USD';

    const { queryByDisplayValue } = render(
      <RatesForDateForm currency={currency} date={date} setQuery={setQuery} />,
    );

    expect(queryByDisplayValue(currencies[currency])).toBeDefined();
  });

  it('should display date', () => {
    const date = new Date('2020/04/01');
    const setQuery = jest.fn();
    const currency: Currency = 'USD';

    const { queryByDisplayValue } = render(
      <RatesForDateForm currency={currency} date={date} setQuery={setQuery} />,
    );

    expect(queryByDisplayValue('2020/04/01')).toBeDefined();
  });

  it('should notify when currency changes', () => {
    const date = new Date('2020/04/10');
    const setQuery = jest.fn();

    const { getByLabelText } = render(
      <RatesForDateForm currency="NZD" date={date} setQuery={setQuery} />,
    );

    const currencyPicker = getByLabelText('Base Currency');
    act(() => {
      fireEvent.change(currencyPicker, { target: { value: 'USD' } });
    });

    expect(setQuery).toHaveBeenCalledWith('USD', date);
  });

  it('should notify when date changes', () => {
    const date = new Date('2020/04/10');
    const setQuery = jest.fn();

    const { getByLabelText } = render(
      <RatesForDateForm currency="NZD" date={date} setQuery={setQuery} />,
    );

    const datePicker = getByLabelText('Date');
    act(() => {
      fireEvent.change(datePicker, { target: { value: '2020/06/09' } });
    });

    expect(setQuery).toHaveBeenCalledWith('NZD', new Date('2020/06/09'));
  });
});
