import React from 'react';
import {
  render,
  act,
  fireEvent,
  waitFor,
} from 'test-utils';
import { mocked } from 'ts-jest/utils';

import { getExchangeRatesForDate, ExchangeRatesForDateResult } from 'api';
import ExchangeRatesForDay from '..';

jest.mock('view/common/CurrencyPicker.tsx');
jest.mock('view/common/DatePicker.tsx');
jest.mock('api');

describe('Rates For Date container', () => {
  beforeEach(() => {
    jest.clearAllMocks();

    mocked(getExchangeRatesForDate)
      .mockResolvedValue([]);
  });

  it('has title "Exchange Rates for a Day"', async () => {
    const { queryByText } = render(
      <ExchangeRatesForDay />,
    );

    expect(queryByText('Exchange Rates for a Day')).toBeDefined();
  });

  it('will fetch from api as soon as it loads', () => {
    const fetchMock = mocked(getExchangeRatesForDate);

    render(
      <ExchangeRatesForDay />,
    );

    expect(fetchMock).toBeCalled();
  });

  it('will fetch from api when picker changes', () => {
    const fetchMock = mocked(getExchangeRatesForDate);

    const { getByLabelText } = render(
      <ExchangeRatesForDay />,
    );

    act(() => {
      fireEvent.change(getByLabelText('Date'), { target: { value: '2020/06/10' } });
    });

    expect(fetchMock).toBeCalledWith(new Date('2020/06/10'), expect.any(String));
  });

  it('will not fetch with just a re-render', () => {
    const fetchMock = mocked(getExchangeRatesForDate);

    const { rerender } = render(
      <ExchangeRatesForDay />,
    );

    expect(fetchMock).toBeCalledTimes(1);

    rerender(
      <ExchangeRatesForDay />,
    );

    expect(fetchMock).toBeCalledTimes(1);
  });

  it('will display the spinner while loading', async () => {
    let resolver: (rates: ExchangeRatesForDateResult) => void;

    const fetchMock = mocked(getExchangeRatesForDate);
    fetchMock.mockReturnValue(new Promise((res) => { resolver = res; }));

    const { queryByTestId, queryByText } = render(
      <ExchangeRatesForDay />,
    );

    expect(queryByTestId('loading-spinner')).toBeInTheDocument();

    // Once resolved, should be undefined
    act(() => resolver([{ currency: 'USD', value: 1.0 }]));
    await waitFor(() => expect(queryByText('US dollar')).toBeInTheDocument());

    expect(queryByTestId('loading-spinner')).not.toBeInTheDocument();
  });

  it('will display an error if one happened', () => {
    const fetchMock = mocked(getExchangeRatesForDate);
    fetchMock.mockRejectedValue(new Error('loading failed for testing reasons'));

    const { queryByText } = render(
      <ExchangeRatesForDay />,
    );

    expect(queryByText(/loading failed for testing reasons/)).toBeDefined();
  });

  it('will not let an older fetch overwrite a newer one', async () => {
    let firstResolver: (rates: ExchangeRatesForDateResult) => void;
    let secondResolver: (rates: ExchangeRatesForDateResult) => void;

    const fetchMock = mocked(getExchangeRatesForDate);
    fetchMock
      .mockReturnValueOnce(new Promise((res) => { firstResolver = res; }))
      .mockReturnValueOnce(new Promise((res) => { secondResolver = res; }));

    const { queryByText, getByLabelText, queryByTestId } = render(
      <ExchangeRatesForDay />,
    );

    // First resolver will have started by this point, but not resolved.

    act(() => {
      fireEvent.change(getByLabelText('Base Currency'), { target: { value: 'EUR' } });
    });

    // Now second resolver will have started

    // Now resolve the second fetch first

    // @ts-ignore - secondResolver HAS been defined.
    if (!firstResolver || !secondResolver) {
      throw new Error('Logic incorrect, both resolvers should be defined');
    }

    secondResolver([{ currency: 'USD', value: 1.0 }]);
    await waitFor(() => expect(queryByTestId('loading-spinner')).not.toBeInTheDocument());

    expect(queryByText('US dollar')).toBeInTheDocument();

    firstResolver([{ currency: 'NZD', value: 0.5 }]);
    await new Promise((res) => setTimeout(res, 200));

    expect(queryByText('New Zealand dollar')).not.toBeInTheDocument();
    expect(queryByText('US dollar')).toBeInTheDocument();
  });

  it('will not let an older fetch overwrite error on a newer one', async () => {
    let firstResolver: (error: Error) => void;
    let secondResolver: (rates: ExchangeRatesForDateResult) => void;

    const fetchMock = mocked(getExchangeRatesForDate);
    fetchMock
      .mockReturnValueOnce(new Promise((res, rej) => { firstResolver = rej; }))
      .mockReturnValueOnce(new Promise((res) => { secondResolver = res; }));

    const { queryByText, getByLabelText, queryByTestId } = render(
      <ExchangeRatesForDay />,
    );

    // First resolver will have started by this point, but not resolved.

    act(() => {
      fireEvent.change(getByLabelText('Base Currency'), { target: { value: 'EUR' } });
    });

    // Now second resolver will have started

    // Now resolve the second fetch first

    // @ts-ignore - secondResolver HAS been defined.
    if (!firstResolver || !secondResolver) {
      throw new Error('Logic incorrect, both resolvers should be defined');
    }

    secondResolver([{ currency: 'USD', value: 1.0 }]);
    await waitFor(() => expect(queryByTestId('loading-spinner')).not.toBeInTheDocument());

    expect(queryByText('US dollar')).toBeInTheDocument();

    firstResolver(new Error('This error should not be visible'));
    await new Promise((res) => setTimeout(res, 200));

    expect(queryByText(/This error should not be visible/)).not.toBeInTheDocument();
    expect(queryByText('US dollar')).toBeInTheDocument();
  });
});
