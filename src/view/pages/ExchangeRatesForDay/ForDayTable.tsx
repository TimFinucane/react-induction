import React from 'react';

import { ExchangeRatesForDateResult } from 'api';
import currencies from 'api/Currencies';
import { displayCurrencyValue } from 'view/TypeDisplays';
import Table from 'view/common/Table';


interface Props {
  exchangeRates: ExchangeRatesForDateResult;
}

export default function ForDayTable({ exchangeRates }: Props) {
  const rows: [string, string][] = exchangeRates
    .sort(({ currency: a }, { currency: b }) => a.localeCompare(b))
    .map(({ currency, value }) => [
      currencies[currency],
      displayCurrencyValue(value, currency),
    ]);

  return (
    <Table columns={['Currency', 'Value']} rows={rows} />
  );
}
