import { Currency } from 'api/Currencies';

export function getLanguage(): string {
  return navigator?.language || 'en-nz';
}

export function displayCurrencyValue(value: number, currency: Currency): string {
  return value.toLocaleString(
    getLanguage(),
    {
      style: 'currency',
      currency,
      currencyDisplay: 'symbol',
      maximumFractionDigits: 2,
      minimumFractionDigits: 2,
    },
  );
}
