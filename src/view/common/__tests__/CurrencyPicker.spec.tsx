import React from 'react';
import { render, within } from '@testing-library/react';
import UserEvent from '@testing-library/user-event';

import { Currency } from 'api/Currencies';
import CurrencyPicker from '../CurrencyPicker';

const selectMaterialUiSelectOption = (element: HTMLElement, optionText: string) => {
  // The the button that opens the dropdown, which is a sibling of the input
  const selectButton = element.parentNode!.querySelector('[role=button]');

  // Open the select dropdown
  UserEvent.click(selectButton!);

  // Get the dropdown element. We don't use getByRole() because it includes <select>s too.
  const listbox = document.body.querySelector('ul[role=listbox]') as HTMLElement;

  // Click the list item
  const listItem = within(listbox).getByText(optionText);
  UserEvent.click(listItem);
};

describe('picker', () => {
  it('does display selected item', () => {
    const { getByText } = render(<CurrencyPicker label="Picker Name" currency="NZD" setCurrency={() => {}} />);

    const title = getByText('Picker Name');
    expect(title).toBeInTheDocument();

    const currentOption = getByText('New Zealand dollar');
    expect(currentOption).toBeVisible();
  });

  it('can change option', async () => {
    const onOptionSelected = jest.fn<void, [Currency]>();

    const { getByLabelText } = render(
      <CurrencyPicker label="Picker Name" currency="NZD" setCurrency={onOptionSelected} />,
    );

    selectMaterialUiSelectOption(getByLabelText('Picker Name'), 'US dollar');

    expect(onOptionSelected).toBeCalledWith('USD');
  });
});
