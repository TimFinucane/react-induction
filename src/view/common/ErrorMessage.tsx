import React from 'react';
import { Alert } from '@material-ui/lab';

interface Props {
  children: string;
}

export default function ErrorMessage({ children }: Props) {
  return <Alert severity="error">{children}</Alert>;
}
