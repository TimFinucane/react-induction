import React from 'react';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';

import currencies, { Currency } from 'api/Currencies';

interface Props {
  id?: string;
  label: string;
  currency: Currency;
  setCurrency(currency: Currency): void;
}

export default function CurrencyPicker({
  id,
  label,
  currency,
  setCurrency,
}: Props) {
  const selectId = id || 'currency-picker';
  const labelId = `${selectId}-label`;

  return (
    <FormControl>
      <InputLabel id={labelId}>{label}</InputLabel>
      <Select
        id={selectId}
        labelId={labelId}
        value={currency}
        onChange={(event) => setCurrency(event.target.value as Currency)}
      >
        {(Object.keys(currencies) as Currency[]).map((code) => (
          <MenuItem key={code} value={code}>{currencies[code]}</MenuItem>
        ))}
      </Select>
    </FormControl>
  );
}
