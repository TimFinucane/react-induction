import React from 'react';
import DateFnsUtils from '@date-io/date-fns';
import { MuiPickersUtilsProvider, DatePicker as MaterialDatePicker } from '@material-ui/pickers';

interface Props {
  id?: string;
  label: string;
  date: Date;
  setDate(date: Date): void;
}

export default function DatePicker({
  id,
  label,
  date,
  setDate,
}: Props) {
  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <MaterialDatePicker
        id={id}
        label={label}
        value={date}
        onChange={(nextDate) => { if (nextDate !== null) setDate(nextDate); }}
        maxDate={new Date()}
      />
    </MuiPickersUtilsProvider>
  );
}
