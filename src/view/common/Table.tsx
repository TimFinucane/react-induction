import React from 'react';
import MaterialTable from '@material-ui/core/Table';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableBody from '@material-ui/core/TableBody';
import { withStyles } from '@material-ui/core/styles';

const HeaderCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.primary.contrastText,
    fontSize: theme.typography.h5.fontSize,
  },
}))(TableCell);

const StripedTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

interface Props {
  columns: [string, string];
  rows: [string, string][];
}

/**
 * A table showing two columns
 */
export default function Table({ columns, rows }: Props) {
  return (
    <MaterialTable>
      <TableHead>
        <TableRow>
          <HeaderCell>{columns[0]}</HeaderCell>
          <HeaderCell>{columns[1]}</HeaderCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {rows.map(([a, b]) => (
          <StripedTableRow key={a}>
            <TableCell>{a}</TableCell>
            <TableCell>{b}</TableCell>
          </StripedTableRow>
        ))}
      </TableBody>
    </MaterialTable>
  );
}
