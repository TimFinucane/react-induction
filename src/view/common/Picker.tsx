import React from 'react';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';

interface Props<T extends string | number> {
  id?: string;
  label: string;
  options: { value: T, name?: string }[];
  value: T;
  setValue(value: T): void;
}

/**
 * A basic drop-down picker of custom supplied options
 */
export default function Picker<T extends string | number>({
  id,
  label,
  options,
  value,
  setValue,
}: Props<T>) {
  const pickerId = id || label;

  return (
    <>
      <InputLabel htmlFor={pickerId}>{label}</InputLabel>
      <Select
        label={label}
        inputProps={{ id: pickerId, 'data-testid': pickerId }}
        value={value}
        onChange={(event) => setValue(event.target.value as T)}
      >
        {options
          .map((option) => (
            <MenuItem
              key={option.value}
              value={option.value}
            >
              {option.name || option.value}
            </MenuItem>
          ))}
      </Select>
    </>
  );
}
