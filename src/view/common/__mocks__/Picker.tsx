import React from 'react';

interface Props<T extends string | number> {
  id?: string;
  label: string;
  options: { value: T, name?: string }[];
  value: T;
  setValue(value: T): void;
}

export default function CurrencyPicker<T extends string | number>({
  id,
  label,
  value,
  setValue,
}: Props<T>) {
  return (
    <div>
      <label htmlFor={id || 'currency-picker'}>{label}</label>
      <input
        id={id || 'currency-picker'}
        value={value}
        onChange={(event) => setValue(event.target.value as T)}
      />
      {value}
    </div>
  );
}
