import React from 'react';

import currencies, { Currency } from 'api/Currencies';

interface Props {
  id?: string;
  label: string;
  currency: Currency;
  setCurrency(currency: Currency): void;
}

export default function CurrencyPicker({
  id,
  label,
  currency,
  setCurrency,
}: Props) {
  return (
    <div>
      <label htmlFor={id || 'currency-picker'}>{label}</label>
      <input
        id={id || 'currency-picker'}
        value={currency}
        onChange={(event) => setCurrency(event.target.value as Currency)}
      />
      {currencies[currency]}
    </div>
  );
}
