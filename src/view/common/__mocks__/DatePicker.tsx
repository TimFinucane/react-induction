import React from 'react';

interface Props {
  id?: string;
  label: string;
  date: Date;
  setDate(date: Date): void;
}

export default function DatePicker({
  id,
  label,
  date,
  setDate,
}: Props) {
  return (
    <div>
      <label htmlFor={id || 'date-picker'}>{label}</label>
      <input
        id={id || 'date-picker'}
        value={date.toLocaleDateString()}
        onChange={(event) => setDate(new Date(event.target.value))}
      />
      {date.toLocaleDateString()}
    </div>
  );
}
