import React from 'react';
import { useHistory } from 'react-router-dom';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

interface Props {
  options: { url: string, name: string }[];
}

/**
 * A navigation bar allowing user to switch pages.
 * Also indicates active page
 *
 * @param props options - a list of urls (starting with /) and their names.
 */
export default function NavBar({ options }: Props) {
  const { location, push } = useHistory();

  // Tabs does not like getting passed a value outside the given set, which can happen if
  // it's not found or '/'
  const hasValidLocation = options.map(({ url }) => url).includes(location.pathname);
  const value = hasValidLocation ? location.pathname : options[0]?.url;

  return (
    <AppBar position="static">
      <Tabs value={value} onChange={(_event, url) => push(url)}>
        {options.map(({ url, name }) => (
          <Tab key={url} value={url} label={name} />
        ))}
      </Tabs>
    </AppBar>
  );
}
