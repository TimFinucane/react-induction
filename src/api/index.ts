import { Currency } from 'api/Currencies';

const API_BASE = 'https://api.exchangeratesapi.io/';

/**
 * @returns string with date format yyyy-mm-dd
 */
function toApiFormat(date: Date) {
  return date.toISOString().split('T')[0];
}

interface HistoryResponse {
  start_at: string; // yyyy-mm-dd
  end_at: string;
  base: string; // 3-letter currency symbol
  rates: {
    [date: string]: {
      [currency: string]: number;
    }
  };
}

export type ExchangeRatesForPeriodResult = { date: Date, value: number }[];

/**
 * @param currency The three-letter currency
 * @param startDate The start date of the period. Defaults to 5 days minus the end date.
 * @param endDate The end date of the period. Defaults to today.
 *
 * @returns a list of exchange rates for the current day
 */
export async function getExchangeRatesForPeriod(
  baseCurrency: Currency,
  compareCurrency: Currency,
  startDate: Date,
  endDate = new Date(),
): Promise<ExchangeRatesForPeriodResult> {
  const response = fetch(`${API_BASE}history/?base=${baseCurrency}&symbols=${compareCurrency}&start_at=${toApiFormat(startDate)}&end_at=${toApiFormat(endDate)}`);

  const body = await (await response).json() as HistoryResponse;

  // Convert the map of currency objects into an array.
  return Object.entries(body.rates)
    .reduce(
      (prev, [date, rate]) => [...prev, { date: new Date(date), value: rate[compareCurrency] }],
      [] as ExchangeRatesForPeriodResult,
    );
}

interface DateResponse {
  date: string; // yyyy-mm-dd
  base: string; // 3-letter currency symbol
  rates: {
    [currency in Currency]: number
  };
}

export type ExchangeRatesForDateResult = { currency: Currency, value: number }[];

export async function getExchangeRatesForDate(
  date = new Date(),
  baseCurrency: Currency,
  currencies?: Currency[],
) : Promise<ExchangeRatesForDateResult> {
  const response = fetch(`${API_BASE}${toApiFormat(date)}/?base=${baseCurrency}${currencies ? `&symbols=${currencies}` : ''}`);

  const body = await (await response).json() as DateResponse;

  // Convert the map of currency objects into an array.
  return (Object.entries(body.rates) as [Currency, number][])
    .reduce(
      (prev, [currency, value]) => [...prev, { currency, value }],
      [] as ExchangeRatesForDateResult,
    );
}
