
/*
 * These are all the currencies available from the UI, as defined in
 * https://www.ecb.europa.eu/stats/policy_and_exchange_rates/euro_reference_exchange_rates/html/index.en.html
 * and https://en.wikipedia.org/wiki/ISO_4217
 */

const currencies = {
  AUD: 'Australian dollar',
  BRL: 'Brazilian real',
  BGN: 'Bulgarian lev',
  CAD: 'Canadian dollar',
  CNY: 'Chinese yuan renminbi',
  HRK: 'Croatian kuna',
  CZK: 'Czech koruna',
  DKK: 'Danish krone',
  EUR: 'Euro',
  HKD: 'Hong Kong dollar',
  HUF: 'Hungarian forint',
  ISK: 'Icelandic krona',
  INR: 'Indian rupee',
  IDR: 'Indonesian rupiah',
  ILS: 'Israeli shekel',
  JPY: 'Japanese yen',
  MYR: 'Malaysian ringgit',
  MXN: 'Mexican peso',
  NZD: 'New Zealand dollar',
  NOK: 'Norwegian krone',
  PHP: 'Philippine peso',
  PLN: 'Polish zloty',
  GBP: 'Pound sterling',
  RON: 'Romanian leu',
  RUB: 'Russian rouble',
  KRW: 'South Korean won',
  SGD: 'Singapore dollar',
  SEK: 'Swedish krona',
  CHF: 'Swiss franc',
  THB: 'Thai baht',
  TRY: 'Turkish lira',
  USD: 'US dollar',
  ZAR: 'South African rand',
};
export default currencies;

export type Currency = keyof typeof currencies;
