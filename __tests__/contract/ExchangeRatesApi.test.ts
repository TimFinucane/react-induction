import { getExchangeRatesForDate, getExchangeRatesForPeriod } from 'api';

const DAY_IN_MILLIS = 24 * 60 * 60 * 1000;

describe('contract exchangeratesapi.io', () => {
  it('should correctly get exchange rates for yesterday', async () => {
    const yesterday = new Date(Date.now() - DAY_IN_MILLIS);

    const response = await getExchangeRatesForDate(yesterday, 'EUR', ['USD', 'JPY']);

    expect(Array.isArray(response)).toBeTruthy();

    if (response.length === 0) {
      // eslint-disable-next-line no-console
      console.warn('Unable to test more about exchange rate query, no results returned');
      return;
    }

    expect(response[0]).toHaveProperty('currency');
    expect(typeof response[0].currency).toEqual(typeof '');
    expect(response[0]).toHaveProperty('value');
    expect(typeof response[0].value).toEqual(typeof 1.0);

    for (const { currency, value } of response) {
      expect(['USD', 'JPY']).toContain(currency);
      expect(typeof value).toEqual(typeof 1.0);
    }
  }, 10000);

  it('should correctly get exchange rates for last 5 days', async () => {
    const startDate = new Date(Date.now() - 5 * DAY_IN_MILLIS);
    const endDate = new Date(Date.now() - DAY_IN_MILLIS);

    const response = await getExchangeRatesForPeriod('EUR', 'USD', startDate, endDate);

    expect(Array.isArray(response)).toBeTruthy();

    if (response.length === 0) {
      // eslint-disable-next-line no-console
      console.warn('Unable to test more about exchange rate query, no results returned');
      return;
    }

    expect(response[0]).toHaveProperty('date');
    expect(response[0].date).toBeInstanceOf(Date);
    expect(response[0]).toHaveProperty('value');
    expect(typeof response[0].value).toEqual(typeof 1.0);
  }, 10000);
});
